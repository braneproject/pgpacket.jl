const S2K_SPECIFIER = Dict([
    (0, "Simple S2K"),
    (1, "Salted S2K"),
    (2, "Reserved value"),
    (3, "Iterated and Salted S2K")
    # 100 to 110 | Private/Experimental S2K
])

struct PrivateKeyPacket  <: PGPBody
    pubkey::PublicKey
    specifics::Array{Any,1}
    PrivateKeyPacket(pubkey, specifics) = new(pubkey, specifics)
end

function show(io::IO, z::PrivateKeyPacket)
    print(io, " ", z.pubkey,
            "\n Specifics : ", z.specifics)
end

function reads2k(io::IOBuffer)
    id = read(io, 1)[1]
    if id == 0x00
        hash_algo = read(io, 1)[1]
        return [id, hash_algo]
    elseif id == 0x03
        hash_algo = read(io, 1)[1]
        salt = read(io, 8)
        c = read(io, 1)[1]
        count = Int32(16) + (c & 15) << ((c >> 4) + 6)
        return [Int(id), HASH_ALGO[hash_algo], bytes2hex(salt), count]
    else
        error("S2K specifier ", id, " not implemented")
    end
end

function bin2privkey(io::IOBuffer, header::PGPHeader)
    pubkey = bin2pubkey(io, header)
    s2k_convention = read(io, 1)[1]
    if s2k_convention in 253:255
        encryption_algo = read(io, 1)[1]
        if s2k_convention == 253
            aead =  read(io, 1)[1]
        end
        s2k_specifier = reads2k(io)
        key = bytes2hex(read(io))
    else
        encryption_algo = 0
        s2k_specifier = nothing
        key = bytes2int(readmpi(io))
    end
    # specifics = [READ_PRIVATE_KEY[pubkey.algo](io)]
    specifics = [s2k_convention, SYMMETRICKEY_ALGORITHM[encryption_algo], s2k_specifier, key]
    return PrivateKeyPacket(pubkey, specifics)
end

function readecdsaprivkey(io::IOBuffer)
    return readmpi(io)
end

READ_PRIVATE_KEY = Dict([
    (0x13, readecdsaprivkey)
])
