function readscalar(bin::Array{UInt8,1})
    T = LENGTH_TYPE[length(bin)]
    if ENDIAN_BOM == 0x04030201
        reverse!(bin)
    end
    return reinterpret(T, bin)[1]
end

function scalar2bytes(x::Unsigned)
    return div(x + 7, 8)
end

function readmpi(io::IOBuffer)
    len = readscalar(read(io, 2))
    return read(io, scalar2bytes(len))
end
