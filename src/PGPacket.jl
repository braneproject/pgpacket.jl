module PGPacket

using ECC
import Base.show
export readheader, bin2packet

include("helper.jl")
include("constants.jl")

abstract type PGPMessage end

struct PGPHeader <: PGPMessage
    packet_tag::Integer
    body_len::Integer
    partial::Bool
    PGPHeader(packet_tag, body_len, partial=false) = new(packet_tag, body_len, partial)
end

function show(io::IO, z::PGPHeader)
    print(io, PACKET[z.packet_tag],
          "\n Length : ", z.body_len, ", partial : ", z.partial,
          "\n ----------------")
end

struct PGPPacket <: PGPMessage
    header::PGPHeader
    body::Any
    PGPPacket(header, body) = new(header, body)
end

function show(io::IO, z::PGPPacket)
    print(io, z.header, "\n", z.body)
end

"""
    IOStream -> PGPHeader

Read PGP message header from IOStream
"""
function readheader(stream::IOStream)
    first = read(stream, 1)[1]
    if first & 0x80 != 0x80
        error("first bit must be one")
    end
    partial = false
    if first & 0x40 == 0x40 # New packet format
        packet_tag = first & 0x3f
        if first < 192
            body_len = Int(first)
        elseif 192 <= first && first < 224
            second = read(stream, 1)
            body_len = ((first - 192) << 8) + second + 192
        elseif 224 <= first && first < 255
            body_len = 1 << (first & 0x1f)
            partial = true
        elseif first == 255
            second, third = read(stream, 1)[1], read(stream, 1)[1]
            fourth, fifth = read(stream, 1)[1], read(stream, 1)[1]
            body_len = (second << 24) | (third << 16) |
                       (fourth << 8)  | fifth
        end
    else # Old packet format
        packet_tag = first >> 2 & 0x0f
        length_type = first & 0x03
        if length_type < 3
            length_bytes = 2^length_type
            body_len = reinterpret(LENGTH_TYPE[length_bytes],
                                   read(stream, length_bytes))[1]
        else
            body_len, partial = 0, true
        end
    end
    PGPHeader(Int(packet_tag), body_len, partial)
end


"""
    binary file -> PGPacket()

Parse PGP message of binary format
"""
function bin2packet(binaryfile::String)
    result = PGPPacket[]
    stream = open(binaryfile)
    while !eof(stream)
        header = readheader(stream)
        body = PACKET_FUNCTIONS[header.packet_tag](IOBuffer(read(stream, header.body_len)), header)
        push!(result, PGPPacket(header, body))
    end
    close(stream)
    return result
end

abstract type PGPBody <: PGPMessage end

function readgenericpacket(io::IOBuffer, header::PGPHeader)
    return bytes2hex(read(io))
end

include("Signature.jl")
include("PublicKey.jl")
include("PrivateKey.jl")

struct UserID <: PGPBody
end

const LENGTH_TYPE = Dict([
    (1, UInt8),
    (2, UInt16),
    (4, UInt32)
])

const PACKET_TYPES = Dict([
    (2,  Signature),
    (6,  PublicKey),
    (13, UserID)
])

const PACKET_FUNCTIONS = Dict([
    (2,  bin2sig),
    (5, bin2privkey),
    (6,  bin2pubkey),
    (13, readgenericpacket) # TODO bin2id
])


end # module
