
const CURVE = Dict([
    ([0x2b, 0x81, 0x04, 0x00, 0x0a], "scep256k1")
])

using Dates: unix2datetime

struct PublicKey <: PGPBody
    version::Integer
    time::Integer
    algo::Integer
    pubkey::Any
    curve::Array{UInt8,1}
    PublicKey(version, time, algo, point, curve=[0x00]) = new(version, time, algo, point, curve)
end

function show(io::IO, z::PublicKey)
    print(io, " Version : ", Int(z.version),
            ", Time : ", unix2datetime(z.time),
            "\n Algorithm : ", PUBLIC_KEY_ALGORITHM[z.algo],
            " using ", CURVE[z.curve],
            "\n ", z.pubkey)
end

"""
Return an hexadecimal representation of the pub key
"""
function readgenericpubkey(io::IOBuffer)
    return bytes2hex(read(io))
end

"""
Return an S256Point from PubKey body
"""
function readecdsapubkey(io::IOBuffer)
    oidlen = read(io, 1)[1]
    curve = read(io, oidlen)
    scalar = readscalar(read(io, 2))
    return [ECC.sec2point(read(io, scalar2bytes(scalar))), curve]
end

"""
    Array{UInt8,1} -> PublicKey

Parse Public-Key from an Array{UInt8,1}
"""
function bin2pubkey(io::IOBuffer, header::PGPHeader)
    version = read(io, 1)[1]
    if version == 3
        time_bytes = read(io, 4)
        time = reinterpret(Int32, reverse!(time_bytes))[1]
        expiry_bytes = read(io, 2)
        expiry = reinterpret(Int16, reverse!(expiry_bytes))[1]
        algo = read(io, 1)[1]
    elseif version == 4
        time_bytes = read(io, 4)
        time = reinterpret(Int32, reverse!(time_bytes))[1]
        algo = read(io, 1)[1]
    end
    pubkey = READ_PUBLIC_KEY[algo](io)
    return PublicKey(version, time, algo, pubkey[1], pubkey[2])
end

const READ_PUBLIC_KEY = Dict([
(1, readgenericpubkey),
(2, readgenericpubkey),
(3, readgenericpubkey),
(16, readgenericpubkey),
(17, readgenericpubkey),
(18, readgenericpubkey),
(19, readecdsapubkey),
(20, readgenericpubkey),
(21, readgenericpubkey),
(22, readgenericpubkey),
(23, readgenericpubkey),
(24, readgenericpubkey)
# 100 to 110 - Private/Experimental algorithm
])
