using Test, PGPacket, ECC

tests = ["ec"]

for t ∈ tests
  include("$(t)test.jl")
end
