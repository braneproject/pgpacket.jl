# PGPacket.jl

[![pipeline status](https://gitlab.com/braneproject/pgpacket.jl/badges/master/pipeline.svg)](https://gitlab.com/braneproject/pgpacket.jl/commits/master)    [![coverage report](https://gitlab.com/braneproject/pgpacket.jl/badges/master/coverage.svg)](https://gitlab.com/braneproject/pgpacket.jl/commits/master)

A Julia PGP message interpreter.

## Usage

```
$ gpg --output pubkey.bin --export 8FAB2B40D753C0F6

$ julia

julia> packets = bin2packet("pubkey.bin")
3-element Array{PGPPacket,1}:
 Public-Key Packet
 Length : 79, Partial : false
PublicKey(0x04, 1550079457, scep256k1 Point(𝑥,𝑦):
f05314566c9bfc8d8cf463a7a01e7735245d588a60dd874f09a9636620abb314,
6bda245d43cbbe019ab1ad74316d675dd858cdd776820969bcc21bbccbd3a661)

 User ID Packet
 Length : 15, Partial : false
626974636f696e2070677020303031
 Signature Packet
 Length : 144, Partial : false
0413130800381621041f6132045b4b6c393c48846e8fab2b40d753c0f605025c6455e1021b03050b0908070206150a09080b020416020301021e01021780000a09108fab2b40d753c0f6b30800ff47b2eee62a229fb9439b80763eb8968ae45d1b13b5a274db2f905cc466c4c4580100efc4ea5b20297efc43a4894248d2d132adf3998d349ac106abfa3a0b27395604

julia> point = ans[1].body.point
scep256k1 Point(𝑥,𝑦):
f05314566c9bfc8d8cf463a7a01e7735245d588a60dd874f09a9636620abb314,
6bda245d43cbbe019ab1ad74316d675dd858cdd776820969bcc21bbccbd3a661

julia> using Bitcoin

julia> address(packets[1].body.point, true, true)
"moZ5AGrmGEFD4rCgSK2Vau46RjjsZpgmNo"
```

## Documentation

https://braneproject.gitlab.io/pgpacket.jl/

## Buy me a cup of coffee

[Donate Bitcoin](bitcoin:34nvxratCQcQgtbwxMJfkmmxwrxtShTn67)
